import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        countries: [],
        locale: "en",
        locales: [],
    },
    getters: {
        countries: state => { return state.countries },
        locale: state => { return state.locale },
        locales: state => { return state.locales },
    },
    mutations: {
        mutationCountries (state, countries) {
            state.countries = countries
        },
        mutationLocale (state, locale) {
            state.locale = locale
        },
        mutationLocales (state, locales) {
            state.locales = locales
        },
    },
    actions: {
        actionCountries ({ commit, state }, countries) {
            commit('mutationCountries', countries)
        },
        actionLocale ({ commit, state }, locale) {
            commit('mutationLocale', locale)
        },
        actionLocales ({ commit, state }, locales) {
            commit('mutationLocales', locales)
        },
    }
})
